    #criacao de uma variavel inteira
	my $inteiro = 3;
	
	#estrutura condicional composta
	if ($inteiro == 3){
		print "3";
	}
	else {
		if ($inteiro == 1){
			print "1";
	    }
		else {
		print "Nao e 1 nem 3";
	    }
	}
	
	
	#estruturas de repeticao (while e for) 
	for(my $i = 0; $i<=10; $i++){
		print $i;
	}
	
	my $incremento = 0;
	while($incremento == 0){
		$incremento++;
		print $incremento;
	}
	
	#criacao de uma funcao 
	
	sub funcao_teste{
		my $primeiro = $_[0];
		my $segundo = $_[1];
		return ($primeiro + $segundo);
	}
	
	print &funcao_teste($incremento, $inteiro);
	
