program Hello;
var inteiro : integer;
var i :integer;
var incremento: integer;
var result: integer;

procedure Soma(inteiro,incremento : integer; VAR resp : integer);
BEGIN
    resp := incremento+inteiro
END;
  
begin
  inteiro := 3;
  incremento := 0;
	
  {estrutura condicional composta}
  if (inteiro = 3) 
  then writeln('3')
  else writeln('1');
  
  {estruturas de repeticao (while e for)}
  for i:=0 to 10 do
    begin
    writeln(i)
    end;
  
  while(incremento = 0) do
      BEGIN
    	incremento+= 1;
    	writeln(incremento)
      END;
	
  Soma(inteiro, incremento, result);
  writeln(result)
end.
