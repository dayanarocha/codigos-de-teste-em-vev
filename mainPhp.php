<?php
/*
 * mainPhp.php
 * 
 * Copyright 2021 Dayana <Dayana@DESKTOP-3QP2FHC>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

    //criacao de uma variavel inteira
	$inteiro = 3;
	
	//estrutura condicional composta
	if ($inteiro == 3){
		echo "3";
	}
	else if ($inteiro == 1){
		echo "1";
	}
	else {
		echo "Nao e 1 nem 3";
	}
	
	//estruturas de repeticao (while e for) 
	$i = 0;
	for($i = 0; $i<=10; $i++){
		echo $i;
	}
	
	$incremento = 0;
	while($incremento == 0){
		$incremento++;
		echo $incremento;
	}
	
	//criacao de uma funcao    
	function funcao_teste(int $incremento, int $inteiro){
		echo ($incremento + $inteiro);
		return 0;
	}
	
	funcao_teste($incremento, $inteiro);
	

?>
<!--
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>sem título</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="generator" content="Geany 1.37.1" />
</head>

<body>
	
</body>

</html>-->
