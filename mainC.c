/*
 * main.c
 * 
 * Copyright 2021 Dayana <Dayana@DESKTOP-3QP2FHC>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <stdio.h>

int main(int argc, char **argv)
{
	
	//criacao de uma variavel inteira
	int inteiro = 3;
	
	//estrutura condicional composta
	if (inteiro == 3){
		printf("3");
	}
	else if (inteiro == 1){
		printf("1");
	}
	else {
		printf("Nao e 1 nem 3");
	}
	
	//estruturas de repeticao (while e for) 
	int i;
	for(i = 0; i<=10; i++){
		printf("%d", i);
	}
	
	int incremento = 0;
	while(incremento == 0){
		incremento++;
		printf("%d", incremento);
	}
	
	//criacao de uma funcao    
	int funcao_teste(int incremento, int inteiro){
		printf("%d", (incremento + inteiro));
		return 0;
	}
	funcao_teste(incremento, inteiro);
	
	return 0;
}

