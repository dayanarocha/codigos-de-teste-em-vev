#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  mainPython.py
#  
#  Copyright 2021 Dayana <Dayana@DESKTOP-3QP2FHC>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


def main(args):
    return 0

if __name__ == '__main__':
    
    #criacao de uma variavel inteira
	inteiro = 3;
	
	#estrutura condicional composta
	if (inteiro == 3):
		print("3");
	
	elif (inteiro == 1):
		print("1");
	
	else:
		print("Nao e 1 nem 3");
	
	
	#estruturas de repeticao (while e for) 
	for i in range(11):
		print(i);

	
	incremento = 0;
	while(incremento == 0):
		incremento+= 1;
		print(incremento);
	
	
	#criacao de uma funcao    
	def funcao_teste(incremento, nteiro):
		print(incremento + inteiro);
		return 0;
	
	funcao_teste(incremento, inteiro);
